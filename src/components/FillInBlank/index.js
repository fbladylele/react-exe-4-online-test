//16.
import { TextField, Typography } from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";

class FillInBlank extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValue: {
        questionId: "",
        answer: {
          content: "",
          exact: "",
        },
      },
    };
  }

  handleChange = async (event) => {
    await this.setState({
      formValue: {
        questionId: this.props.item.id,
        answer: {
          content: event.target.value,
          exact:
            event.target.value &&
            event.target.value.toLowerCase() ===
              this.props.item.answers[0].content.toLowerCase(),
        },
      },
    });

    console.log(this.state.formValue);
    this.props.dispatch(
      createAction(actionType.SET_ANSWER, this.state.formValue)
    );
  };

  render() {
    const { id, content } = this.props.item;
    return (
      <div>
        <Typography>
          Câu hỏi {id}: {content}
        </Typography>
        <TextField
          onBlur={this.handleChange}
          name={content}
          fullWidth
          label=""
          variant="outlined"
          style={{ marginBottom: 10 }}
        />
      </div>
    );
  }
}

export default connect()(FillInBlank);

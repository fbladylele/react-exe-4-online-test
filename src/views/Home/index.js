import React, { Component } from "react";
import MulChoice from "../../components/MulChoice";
import FillInBlank from "../../components/FillInBlank";
import { Button, Container, Typography } from "@material-ui/core";
import { fetchQuestions } from "../../store/actions/questions";
import { connect } from "react-redux";

class Home extends Component {
  //15.
  renderQuestions = () => {
    return this.props.questions.map((item) => {
      if (item.questionType === 1) {
        return (
          <div key={item.id}>
            <Typography>
              Câu hỏi {item.id}: {item.content}
            </Typography>
            {item.answers.map((answer) => {
              return <MulChoice key={answer.id} item={item} answer={answer} />;
            })}
          </div>
        );
      } else {
        return (
          <div key={item.id}>
            <FillInBlank item={item} />
          </div>
        );
      }
    });
  };

  handleSubmit = () => {
    let result = 0;
    let arrAnswer = Object.values(this.props.answers);
    for (const item of arrAnswer) {
      if (item.answer.exact) {
        result++;
      }
    }
    alert(
      `trời wơ bạn đúng ${result} / ${this.props.questions.length} câu, dữ hông!`
    );
  };

  render() {
    return (
      <div>
        <Typography
          component="h1"
          variant="h4"
          align="center"
          style={{ padding: 20 }}
        >
          TRẮC NGHIỆM GIÁO DỤC CÔNG DÂN
        </Typography>
        <Container maxWidth="lg">
          <form onSubmit={this.handleSubmit}>
            {this.renderQuestions()}
            <div>
              <Button type="submit" variant="contained" color="primary">
                Nộp bài
              </Button>
            </div>
          </form>
        </Container>
      </div>
    );
  }
  async componentDidMount() {
    this.props.dispatch(fetchQuestions);
  }
}

//.14:
const mapStateToProps = (state) => {
  return {
    questions: state.questionReducer.questions,
    answers: state.questionReducer.answers,
  };
};
export default connect(mapStateToProps)(Home);
